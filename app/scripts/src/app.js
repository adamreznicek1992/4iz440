
// import libs
import cookieBar from './libs/cookieBar';

window.ds = {};

var $header = $('#header');
var headerHeight = $header.height();

// import components
// import './components/ie-edge-detection.js';
import './components/menu.js';
import './components/sparql.js';

// init
// cookieBar.init({
//   // settings
//   // infoText: 'Soubory cookies nám pomáhají poskytovat, chránit a zlepšovat naše služby. Prohlížením tohoto webu s jejich používáním souhlasíte.',
//   // buttonText: 'Rozumím',
//   ipCheck: false,
// });

cookieBar.init({
  // settings
  // infoText: 'Soubory cookies nám pomáhají poskytovat, chránit a zlepšovat naše služby. Prohlížením tohoto webu s jejich používáním souhlasíte.',
  // buttonText: 'Rozumím',
  ipCheck: false,
});

// ----------------------------------------
//  scroll to
function scrollTo(e) {
  e.preventDefault();

  var href = $.attr(this, 'href');
  if (typeof href === 'undefined') {
      href = $.attr(this, 'data-href');
  }
  var offset = $.attr(this, 'data-offset');

  var $target = $(href);
  if ($target.length) {
      $('html,body').animate({
        scrollTop: $target.offset().top - headerHeight + ((typeof offset !== 'undefined') ? parseInt(offset) : 10)
      }, {
        duration: 500,
        easing: 'swing'
      });
  }
}

var $scrollToItems = $('.scrollTo');
$.each($scrollToItems, function() {
  $(this).on('click', scrollTo);
});
// ----------------------------------------

// scripts here