

// mobile menu scripts
;(function ($) {

  "use strict";

  var mobileBreakpoint = '767px';

  var $headerSticky = $('#header');
  var headerHeight = $headerSticky.height();

  // resize fix
  var rtime =  null;
  var timeout = false;
  var delta = 500;

  function isMobile(breakpoint) {
    return window.matchMedia('(max-width: ' + breakpoint + ')').matches;
  }

  // burger
  $('.nav__toggle').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('is-active');
    $('body').toggleClass('menu-active');
  });

  $('.nav__item').on('click', function(e) {
    $('.nav__toggle').removeClass('is-active');
    $('body').removeClass('menu-active');
  });

})(jQuery);
