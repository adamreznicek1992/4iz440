
// variables
let endpoint = 'http://linked.opendata.cz/sparql';
let dataset = 'http://linked.opendata.cz/resource/dataset/ares/rzp';


// Parse a SPARQL query to a JSON object
var SparqlParser = require('sparqljs').Parser;
var parser = new SparqlParser();

// JSON queries
var exploreQuery = parser.parse(
  'select distinct ?subject ?property ?object ' +
  'where {?subject ?property ?object} ' +
  'LIMIT 100 ' +
  'OFFSET 0 '
);
console.log(exploreQuery);


// Regenerate a SPARQL query from a JSON object
var SparqlGenerator = require('sparqljs').Generator;
var generator = new SparqlGenerator();

// String queries
var stringExploreQuery = generator.stringify(exploreQuery);
// console.log(stringExploreQuery);



// var sparql = "select distinct ?class where {?s a ?class} LIMIT 10 OFFSET 0";
var query = endpoint + '/?default-graph-uri=' + dataset + '&query=' + escape(stringExploreQuery) + '&format=json';
console.log(query);

// AJAX request
$.ajax({
    url: query,
    dataType: 'jsonp',
    jsonp: 'callback',
    success: function(data) {
      console.log(data.results.bindings);
    }
});