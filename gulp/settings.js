
// Settings
const settings = {
  static: false,
};

// Paths
const tempDir = '.tmp';
const templateDevDir = 'app';
const distDir = 'dist';

const swigDevDir = templateDevDir + '/views';
const cssTempDir = tempDir + '/styles';
const cssDistDir = distDir + '/styles';
const jsDevDir = templateDevDir + '/scripts';
const jsTempDir = tempDir + '/scripts';
const jsDistDir = distDir + '/scripts';
const imgDevDir = templateDevDir + '/images';
const fontsDevDir = templateDevDir + '/fonts';
const fontsDistDir = distDir + '/fonts';
const dataDevDir = templateDevDir + '/datas';