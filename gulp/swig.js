
// Paths
const tempDir = '.tmp';
const templateDevDir = 'app';
const distDir = 'dist';

const swigDevDir = templateDevDir + '/views';
const dataDevDir = templateDevDir + '/datas';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

// template consts
const projectName = "4IZ440 - Vizualizační aplikace";
const year = new Date().getFullYear();
const timestamp = new Date().getTime();

gulp.task('templates', function() {
  var options = {
    load_json : true,
    json_path: dataDevDir,
    data: {
      projectName: projectName,
      year: year,
      timestamp: timestamp,
      path: '/'
    },
    defaults: {
      'cache': false
    }
  };

  return gulp.src([
    swigDevDir + '/**/*.swig'
    ])
    .pipe($.plumber())
    .pipe($.swig(options))
    .pipe(gulp.dest(tempDir))
    .pipe($.livereload());
});