4IZ440 - Vizualizační aplikace dat https://linked.opendata.cz/dataset/mfcr-ares-rzp
=========

Projekt má za cíl vizualizovat jednotlivé živnostenské subjekty prostřednictvím Google Maps API.
<br>
Uživatel má možnost filtrovat subjekty pomocí kraje a oboru podnikání.
<br>
<br>
Práce slouží jako demostrace využití RDF v praxi.

How to start develop?
--------

Install dev-stack:

	composer install
    npm install (or yarn install)
    gulp



#### Run dev-stack:

    gulp watch (start watch with liveReload)